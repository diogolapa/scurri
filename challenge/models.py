from django.db import models
from django.contrib.auth.models import AbstractUser


class CarrierAccountAlreadyExists(Exception):
    """Raised when a carrier account already exists for the user and carrier type"""
    pass


class AbstractCarrierAccountException(Exception):
    """Raised when a method from a base carrier account instance is called"""
    pass


class DateAwareModel(models.Model):
    """Make our models date aware"""
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Carrier(DateAwareModel):
    """Carrier model"""
    type = models.CharField(max_length=200, unique=True)
    # Other fields...

    def __unicode__(self):
        return self.identifier


class CarrierAccountManager(models.Manager):
    """Carrier accounts manager
    Works as our factory for carrier accounts
    """
    def create(self, user, **kwargs):
        """Returns an instance of a carrier account
        Get carrier for the type associated with the class used to call this method
        The user account is passed by argument
        """
        carrier, _ = Carrier.objects.get_or_create(type=self.model.carrier_type())

        if self.model.objects.filter(user_account=user).exists():
            raise CarrierAccountAlreadyExists

        carrier_account = self.model(carrier=carrier, user_account=user, **kwargs)
        carrier_account.save()

        return carrier_account


class CarrierAccount(DateAwareModel):
    """Base class for carrier accounts"""
    carrier = models.ForeignKey('Carrier')
    user_account = models.ForeignKey('Account')

    objects = CarrierAccountManager()

    @classmethod
    def carrier_type(cls):
        """Returns carrier type"""
        if hasattr(cls, '_carrier_type'):
            return cls._carrier_type
        else:
            raise AbstractCarrierAccountException

    def request_headers(self):
        """Abstract method that returns specific header data for a carrier account"""
        raise AbstractCarrierAccountException


def carrier_type(identifier):
    """Class decorator to identify carrier type"""
    def class_decorator(cls):
        setattr(cls, '_carrier_type', identifier)
        return cls
    return class_decorator


@carrier_type('DHL')
class DhlCarrierAccount(CarrierAccount):
    """DHL specific carrier account"""
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=128)

    objects = CarrierAccountManager()

    def request_headers(self):
        return {
            'X-Username': self.username,
            'X-Password': self.password
        }


@carrier_type('UPS')
class UpsCarrierAccount(CarrierAccount):
    """UPS specific carrier account"""
    client_id = models.CharField(max_length=200)

    objects = CarrierAccountManager()

    def request_headers(self):
        return {
            'X-ClientId': self.client_id
        }


class Account(AbstractUser):
    """User account"""
    pass


class CarrierAccountRequestDecorator:
    """Extends headers property with header fields given by the carrier account passed"""
    def __init__(self, decorated, carrier_account):
        self.decorated = decorated
        self.carrier_account = carrier_account

    def __getattr__(self, name):
        if name == 'headers':
            result = self.decorated.headers.copy()
            result.update(self.carrier_account.request_headers())
            return result
        else:
            return CarrierAccountRequestDecorator.__getattr__(self, name)
