from django.contrib import admin

from challenge.models import Account, Carrier

admin.site.register(Account)
admin.site.register(Carrier)