from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase

from challenge.models import Account, UpsCarrierAccount, DhlCarrierAccount, CarrierAccountAlreadyExists, \
    CarrierAccountRequestDecorator, CarrierAccount, AbstractCarrierAccountException


# Request Mock class
class Request():
    def __init__(self):
        self.headers = {}


class CarrierAccountTestCase(TestCase):
    def setUp(self):
        self.user1 = Account.objects.create_user('user1', 'user1@domain.com', 'password')
        self.user2 = Account.objects.create_user('user2', 'user2@domain.com', 'password')

        UpsCarrierAccount.objects.create(self.user2, client_id='user_client_id')

    def test_carrier_creation(self):
        # Check that we can't create an abstract CarrierAccount
        with self.assertRaises(AbstractCarrierAccountException):
            CarrierAccount.objects.create(self.user2)

        # Create one UPS account with our CarrierAccount factory
        ups = UpsCarrierAccount.objects.create(self.user1, client_id='user_client_id')

        # Check that carrier specific data is there
        self.assertEqual(ups.client_id, 'user_client_id')

        # Check that the carrier account is saved in database
        self.assertEqual(ups, UpsCarrierAccount.objects.get(user_account=self.user1))

        # Assuming each user can only have one account for each carrier
        with self.assertRaises(CarrierAccountAlreadyExists):
            UpsCarrierAccount.objects.create(self.user1, client_id='user_client_id2')

    def test_list_account_carrier_accounts(self):
        # Create two different accounts for user1
        UpsCarrierAccount.objects.create(self.user1)
        DhlCarrierAccount.objects.create(self.user1)

        # Check if both are accessible
        self.assertEqual(CarrierAccount.objects.filter(user_account=self.user1).count(), 2)

    def test_get_specific_carrier_account(self):
        # Get UPS account for user2 created on setup
        self.assertIsNotNone(UpsCarrierAccount.objects.get(user_account=self.user2))

        # Check that trying to get an inexistent account raises an exception
        with self.assertRaises(ObjectDoesNotExist):
            DhlCarrierAccount.objects.get(user_account=self.user2)

    def test_request_builder(self):
        # Create a mock HTTP request
        request = Request()

        # Add UPS carrier account options with our decorator
        authenticated_request = CarrierAccountRequestDecorator(
            request,
            UpsCarrierAccount.objects.get(user_account=self.user2)
        )

        # Check that specific field is there
        self.assertIn('X-ClientId', authenticated_request.headers.keys())
